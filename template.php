<?php
function sands_breadcrumb($breadcrumb)
{
  return implode(' &raquo; ', $breadcrumb);
}

function sands_links($links, $delimiter = ' &bull;&nbsp;')
{
  if (!is_array($links)) {
    return '';
  }

  /* Maps a link to it's associated class */
  $link_to_id = array(
    t('edit') => 'edit',
    t('delete') => 'delete',
    t('Add a new comment to this page') => 'add-comment',
    t('Share your thoughts and opinions related to this posting') => 'add-comment',
    t('Jump to the first comment') => 'comment',
    t('reply') => 'reply',
    //t('read more') => 'read-more',
    t('printer-friendly version') => 'print',
    t('add child page') => 'add-child-page',
    t('calendar') => 'calendar',
  );

  $new_links = array();

  foreach ($links as $link) {
    foreach ($link_to_id as $text => $id) {
      if (strpos($link, $text)) {
        $link = str_replace('<a ', "<a class=\"icon-$id\" ", $link);
        break;
      }
    }
    $new_links[] = $link;
  }

  return implode($delimiter, $new_links);
}

function sands_regions()
{
  return array(
    'left' => t('left sidebar'),
    'right' => t('right sidebar')
  );
}

// vim: et ts=2 sw=2:
?>

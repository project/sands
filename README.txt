Description
===========
Sands is an attempt to create a "generic" Drupal theme in the vein of
Drupal's classic Bluemarine theme, updated to look more "modern" and
easily act as a Bluemarine replacement. It has design attributes taken 
from Drupal.org's proprietary Bluebeach theme and the friendselectric theme.
Some features:

* Higher contrast colors, underlined links, and relative font sizes
* Novel styling of node information, comments, and block menus
* Semantic XHTML, (mostly) tableless, and heavy use of CSS (including an 
  "elastic"/fluid, 3-column CSS layout)
* Segregation of style information, including an easily-modifiable colors.css
  containing all color and background image definitions
* Use of many subtle gradiented background images to create a "smooth," more
  high-contrast look
* Use of the PHPTemplate theme engine.


Authors
=======
This theme was designed by Samat Jain (http://tamasrepus.rhombic.net/).

Known Issues
============
- Please see Drupal's issue tracker for this theme at 
  http://drupal.org/project/issues/sands

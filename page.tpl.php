<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body<?php print $onload_attributes ?>>
<div id="pageWrapper">

<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td>
      <table>
        <tr>
          <?php if ($logo) { ?><td id="logo"><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home'); ?>" /></a></td><?php } ?>
          <td id="site-name-slogan">
          <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>"><?php print $site_name ?></a></h1><?php } ?>
          <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
          </td>
        </tr>
      </table>
    </td>
    <td id="menu">
      <?php if ($secondary_links) { ?><div id="secondary"><?php print theme('links',$secondary_links) ?></div><?php } ?>
      <?php if ($primary_links) { ?><div id="primary"><?php print theme('links',$primary_links) ?></div><?php } ?>
      <?php if ($search_box) { print $search_box; } ?>
    </td>
  </tr>
</table>

<table id="columnContainer">
  <tr>
    <?php if ($sidebar_left) { ?>
    <td id="leftColumn">
      <div class="sidebar" id="sidebar-left"><?php print $sidebar_left ?></div>
      <div class="clear"></div>
    </td>
    <?php } ?>
    <td id="middleColumn">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php if ($breadcrumb) print "<div class=\"breadcrumb\">$breadcrumb &raquo; $title</div>"; ?>
        <?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>
        <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
        <?php print $help ?>
        <?php print $messages ?>

        <?php print $content; ?>
      </div>
      <div class="clear"></div>
    </td>
    <?php if ($sidebar_right) { ?>
    <td id="rightColumn">
      <div class="sidebar" id="sidebar-right"><?php print $sidebar_right ?></div>
      <div class="clear"></div>
    </td>
    <?php } ?>
  </tr>
</table>

<div id="footer">
  <?php print $footer_message ?>
  <p>Some portions of this website's design are part of the <a href="http://samat.org/drupal-themes/sands">Sands theme</a> for <a href="http://drupal.org">Drupal</a>, which is <a href="http://www.gnu.org/copyleft/">copyleft</a> (c) 2005-2006 <a href="http://samat.org/">Samat Jain</a>. All rights reserved.</p>
</div>
<?php print $closure ?>
</div>
</body>
</html>
